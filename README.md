# Panorama3D

Visionneuse d'images à 360° 

Démonstration : [Site Panorama3D](https://cogito.no-ip.info/cogito/panorama3d/)

# Installation 

* Copier le dossier du site dans le dossier du serveur web
* Il suffit d'ajouter des vues à 360° toutes au même format et actualiser la page du site. Les images se chargent automatiquement.

# Licence 
* voir le fichier Licence
* Utilisation de la bibliothèque javascript Three.js : Licence MIT
* Utilisation du fichier W3.CSS 4.15 December 2020 by Jan Egil and Borge Refsnes
