
/* Adaptation de l'exemple webgl_panorama_equirectangular.html
https://github.com/mrdoob/three.js/blob/master/examples/webgl_panorama_equirectangular.html 
Utilisation d'image HDR 4096x2048
*/

let nomImage;
let camera, scene, renderer;
let isUserInteracting = false,
				onPointerDownMouseX = 0, onPointerDownMouseY = 0,
				lon = 0, onPointerDownLon = 0,
				lat = 0, onPointerDownLat = 0,
				phi = 0, theta = 0;


var material;
var texture ;  
var mesh;
var fichiersImage ;
var index = 0 ;
var nbrImages = 0;

function passerImages(json)
{
  fichiersImage = json.sort();
  init();
  animate();
}

function init() 
{
		 	
	largeur = 	window.innerWidth*10/10;
   hauteur = 	window.innerHeight*9/10;
   
	const container = document.getElementById( 'container' );
	
   camera = new THREE.PerspectiveCamera( 85, window.innerWidth / window.innerHeight, 1, 1100 );
   
   scene = new THREE.Scene();
   
   const geometry = new THREE.SphereGeometry( 500, 150, 40 );
	// inverser la géométrie sur l'axe x de sorte que toutes les faces pointent vers l'intérieur.
	geometry.scale( -1, 1, 1 );
   texture = new THREE.TextureLoader().load( 'images/'+fichiersImage[0] );
   material = new THREE.MeshBasicMaterial( { map: texture } );

   mesh = new THREE.Mesh( geometry, material );
   scene.add( mesh );
   
   renderer = new THREE.WebGLRenderer();
   renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setSize( largeur, hauteur );
	container.appendChild( renderer.domElement );

	var dibNbrImg = document.getElementById('lignes');
 	nbrImages = dibNbrImg.innerHTML;
 	
	var nombre = document.getElementById( 'nombre' );
	nombre.innerText = nbrImages;
	
   container.style.touchAction = 'none';
	container.addEventListener( 'pointerdown', onPointerDown );

	document.addEventListener( 'wheel', onDocumentMouseWheel );	
	
	document.getElementsByTagName("canvas")[0].className='w3-border  w3-card-4'; 
}


	function onWindowResize(largeur, hauteur) {
	   
		camera.aspect = window.innerWidth / window.innerHeight;
		camera.updateProjectionMatrix();
	
		renderer.setSize( largeur, hauteur );
	
	}
	
	function onPointerDown( event ) {
	
		if ( event.isPrimary === false ) return;
	
		isUserInteracting = true;
	
		onPointerDownMouseX = event.clientX;
		onPointerDownMouseY = event.clientY;
	
		onPointerDownLon = lon;
		onPointerDownLat = lat;
	
		document.addEventListener( 'pointermove', onPointerMove );
		document.addEventListener( 'pointerup', onPointerUp );
	
	}
	
	function onPointerMove( event ) {
	
		if ( event.isPrimary === false ) return;
	
		lon = ( onPointerDownMouseX - event.clientX ) * 0.1 + onPointerDownLon;
		lat = ( event.clientY - onPointerDownMouseY ) * 0.1 + onPointerDownLat;
	
	}
	
	function onPointerUp() {
	
		if ( event.isPrimary === false ) return;
	
		isUserInteracting = false;
	
		document.removeEventListener( 'pointermove', onPointerMove );
		document.removeEventListener( 'pointerup', onPointerUp );
	
	}
	
	function onDocumentMouseWheel( event ) {
	
		const fov = camera.fov + event.deltaY * 0.05;
	
		camera.fov = THREE.MathUtils.clamp( fov, 10, 75 );
	
		camera.updateProjectionMatrix();
	
	}
	
	function animate() {
	
		requestAnimationFrame( animate );
		update();
	
	}
	
function update() {
	
		if ( isUserInteracting === false ) {
	
			lon += 0.1;
	
		}
	
		lat = Math.max( - 85, Math.min( 85, lat ) );
		phi = THREE.MathUtils.degToRad( 90 - lat );
		theta = THREE.MathUtils.degToRad( lon );
	
		const x = 500 * Math.sin( phi ) * Math.cos( theta );
		const y = 500 * Math.cos( phi );
		const z = 500 * Math.sin( phi ) * Math.sin( theta );
	
		camera.lookAt( x, y, z );
	
		renderer.render( scene, camera );
	
}

//Diaporama 	
function avant() 
{
	index--;
	if(index < 0 )
	{
		index = nbrImages-1;
	}
	texture.source.data.src = 'images/'+fichiersImage[index]; //Il faudrait sans doute charger l'image autrement
   mesh.material.map.needsUpdate = true;
}
	
function apres() 
{
  	index++;
	if(index >= nbrImages)
	{
		index = 0;
	}
	texture.source.data.src = 'images/'+fichiersImage[index]; //Il faudrait sans doute charger l'image autrement
   mesh.material.map.needsUpdate = true;
}


	