<?php
   require("includes/fonctions.php");
   $liste = listerFichiers("images/"); 
   $json = json_encode($liste);
?>
<!DOCTYPE html>
 <html>
	 <head>
		<title>Panorama -- visionneuse </title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
		<link rel="stylesheet" href="css/w3.css">
		<link rel="stylesheet" href="css/style.css">
	 </head>
	 <body onload = 'passerImages(<?php echo $json; ?>)'>
	    <div  id ="Afficher">
	        <div >
			         <div id="lignes"><?php  echo count($liste);?></div>
			         <?php include( 'includes/diaporama.php'); ?><br>
  	        </div>  	        
		  </div>
     </body>	
	<footer class='footer '> 
		<div class='w3-tiny' style='padding-left:20px;'>
			<br><a rel='license' href='https://creativecommons.org/licenses/by-nc/4.0/deed.fr'>
			<img alt='Licence Creative Commons' style='border-width:0' src='https://licensebuttons.net/l/by-nc/4.0/88x31.png' /></a>
			Acryline-Erin -- Licence Creative Commons Attribution - Pas d’Utilisation Commerciale 4.0 France</a>.
		</div>
	</footer>
      <script src="js/three.js"></script>
		<script src="js/index.js"></script>
</html>
