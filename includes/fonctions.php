<?php

function listerFichiers($dossier)
{
  $marque =  opendir($dossier) or die('Erreur');
  $liste = array();
  while($entree = @readdir($marque))  
  {
  	 if( $entree != '.' && $entree != '..'  )
  	   {
  	   	$tab = explode(".",$entree);
  	   	if($tab[1])
  	   	{
  	   		if(trim($tab[1]) === "jpg" || trim($tab[1]) === "png")
  	   		{
  	  	   	   array_push($liste,trim($entree));
  	  	   	}
  	  	   }
  	   }
  }
 return $liste;
}