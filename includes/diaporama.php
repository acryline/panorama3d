
<div id="info" >
	 <div class="w3-xlarge">Visionneuse de <span id="nombre"> </span> captures 360° de simulations 
	 <a href="http://opensimulator.org/wiki/Scripting_Documentation" target="_blank" > OpenSimulator</a><br>
	 <span class="w3-tiny">Utilisez la souris pour orienter et zoomer l'image.</span></div>
</div>
<div class= "diapo w3-display-container">
	<button class="w3-jumbo btn w3-display-left w3-text-white boutons" id="avant" onclick="avant();"><</button>
	<button  class="w3-jumbo btn w3-display-right w3-text-white boutons" id="apres" onclick="apres();">></button>
	<div class="images" id="container"></div>
</div>
